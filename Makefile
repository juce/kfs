# For release builds, use "debug=1" in command line. For instance,
# to build DLLs in release mode: nmake dlls debug=1

CC=cl
LINK=link
RC=rc

DOKAN_INC=soft\dokan
DOKAN_LIB=soft\dokan 
D3DX9=d3dx9.lib

!if "$(debug)"=="1"
EXTRA_CFLAGS=/DDEBUG
!else
EXTRA_CFLAGS=/DMYDLL_RELEASE_BUILD
!endif

# 4731: warning about ebp modification
CFLAGS=/nologo /O2 /EHsc /wd4731 $(EXTRA_CFLAGS)
LFLAGS=/NOLOGO /LIBPATH:$(DOKAN_LIB)
LIBS=user32.lib gdi32.lib advapi32.lib comctl32.lib shlwapi.lib
LIBSDLL=pngdib.obj libpng.a zdll.lib $(LIBS)


all: kfs.exe
	
kfs.obj: kfs.cpp kfs.h

kfs.exe: kfs.obj kfs.res
	$(LINK) $(LFLAGS) /out:kfs.exe kfs.obj kfs.res dokan.lib $(LIBS)
kfs.res: kfs.rc kfs.ico
	$(RC) -r -fo kfs.res kfs.rc


.cpp.obj:
	$(CC) $(CFLAGS) -c $(INC) /I $(DOKAN_INC) $<

clean:
	del /Q /F *.exp *.lib *.obj *.res *.dll *.exe *~
	if exist output (rd /S /Q output)

