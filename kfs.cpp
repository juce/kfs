#define UNICODE

#include <windows.h>
#include <winbase.h>
#include <stdio.h>
#include <stdlib.h>
#include <shlwapi.h>

#include <hash_map>
#include <list>
#include <string>

#include "dokan.h"
#include "afsreader.h"
#include "kfs.h"

using namespace std;
using namespace stdext;

HWND hWnd = NULL;

bool g_UseStdErr(false);
bool g_DebugMode(false);

CRITICAL_SECTION cs;
CRITICAL_SECTION logging_cs;

class lock_t 
{
public:
    CRITICAL_SECTION* _pcs;
    lock_t(CRITICAL_SECTION* pcs) : _pcs(pcs) {
        EnterCriticalSection(_pcs);
    }
    ~lock_t() {
        LeaveCriticalSection(_pcs);
    }
};


static void string_strip_quotes(wstring& s)
{
    static const wchar_t* chars = L" \t\n\r\"'";
    int e = s.find_last_not_of(chars);
    s.erase(e + 1);
    int b = s.find_first_not_of(chars);
    s.erase(0,b);
}

static void GetAbsolutePath(const wstring& v, wstring& s)
{
    if (PathIsRelative(v.c_str())) {
        wchar_t path[MAX_PATH], canonical_path[MAX_PATH];
        GetCurrentDirectory(MAX_PATH, path);
        PathAppend(path, v.c_str());
        PathCanonicalize(canonical_path, path);
        s = canonical_path;
    }
    else {
        s = v;
    }
}

class config_t {
public:
    bool _debug;
    wstring _log_file;
    wstring _section_name;
    int _fileNameLen;
    bool _extend_afs;
    wstring _afs_source;
    wstring _kfs_mount;
    bool _dokan_removable_drive;
    int _dokan_thread_count;
    list<wstring> _kfs_roots;

    config_t(const wstring& section_name, const wchar_t* config_ini) : 
                 _section_name(section_name),
                 _fileNameLen(63), 
                 _extend_afs(false),
                 _debug(false),
                 _dokan_removable_drive(false),
                 _dokan_thread_count(0)
    {
        wchar_t settings[32767];
        RtlZeroMemory(settings, sizeof(settings));
        GetPrivateProfileSection(_section_name.c_str(),
            settings, sizeof(settings)/sizeof(wchar_t), config_ini);

        wchar_t* p = settings;
        while (*p) {
            wstring pair(p);
            wstring key(pair.substr(0, pair.find(L"=")));
            wstring value(pair.substr(pair.find(L"=")+1));
            string_strip_quotes(value);

            if (wcscmp(L"afs.source", key.c_str())==0) {
                GetAbsolutePath(value, _afs_source);
            }
            else if (wcscmp(L"kfs.root", key.c_str())==0) {
                wstring root;
                GetAbsolutePath(value, root);
                _kfs_roots.push_back(root);
            }
            else if (wcscmp(L"kfs.mount", key.c_str())==0) {
                GetAbsolutePath(value, _kfs_mount);
            }
            else if (wcscmp(L"log.file", key.c_str())==0) {
                GetAbsolutePath(value, _log_file);
            }

            p += wcslen(p) + 1;
        }

        _extend_afs = GetPrivateProfileInt(_section_name.c_str(),
            L"afs.extend", _extend_afs,
            config_ini);

        _dokan_thread_count = GetPrivateProfileInt(_section_name.c_str(),
            L"dokan.thread_count", _dokan_thread_count,
            config_ini);

        _dokan_removable_drive = GetPrivateProfileInt(_section_name.c_str(),
            L"dokan.removable_drive", _dokan_removable_drive,
            config_ini);

        _debug = GetPrivateProfileInt(_section_name.c_str(),
            L"debug", _debug,
            config_ini);
    }
};

list<config_t*> _config_list;
int _thread_count(0);

#define AFS_HEADER_AND_TOS_SIZE (((((\
    sizeof(AFSDIRHEADER)+0x10000*sizeof(AFSITEMINFO))-1)>>0x0b)+1)<<0x0b)

#define PAGED(x) (((((x)-1)>>0x0b)+1)<<0x0b)

typedef struct _kfs_directory_entry_t {
    wchar_t filename[64];
    list<wstring>::iterator kfs_root;
    DWORD afs_offset;
    DWORD file_size;
    DWORD file_size_aligned;
    DWORD afs_original_offset;
    DWORD afs_original_size;
} kfs_directory_entry_t;

typedef struct _kfs_afs_header_t {
    union {
        struct {
            AFSDIRHEADER header;
            AFSITEMINFO tos[0x10000];
        } data;
        BYTE bytes[AFS_HEADER_AND_TOS_SIZE];
    };
} kfs_afs_header_t;

class kfs_mapping_t
{
public:
    bool is_file;
    LARGE_INTEGER size;
    wstring path;

    kfs_mapping_t() : is_file(true) {
        size.QuadPart = 0;
    }
};

class kfs_directory_t : public kfs_mapping_t
{
public:
    kfs_directory_entry_t entries[0x10000];
    kfs_afs_header_t header_and_tos;
    bool up_to_date;

    kfs_directory_t() : up_to_date(false) {
        RtlZeroMemory(entries, sizeof(entries));
        RtlZeroMemory(&header_and_tos, sizeof(header_and_tos));
        is_file = false;
    }
};

class afs_names_t
{
public:
    bool loaded;
    hash_map<wstring,int> _names;
    afs_names_t() : loaded(false) {}
    int find(const wchar_t* name) {
        hash_map<wstring,int>::iterator it = _names.find(name);
        if (it != _names.end()) {
            return it->second;
        }
        return -1;
    }
};

static void DbgPrint(LPCWSTR format, ...)
{
    //lock_t lock(&logging_cs);
    wchar_t _buffer[512];
    va_list argp;
    va_start(argp, format);
    vswprintf_s(_buffer, sizeof(_buffer)/sizeof(WCHAR), format, argp);
    va_end(argp);
    if (g_UseStdErr) {
        SYSTEMTIME t;
        GetLocalTime(&t);
        fwprintf(stderr, L"[%04d-%02d-%02d %02d:%02d:%02d.%03d] ", 
                t.wYear, t.wMonth, t.wDay, 
                t.wHour, t.wMinute, t.wSecond,
                t.wMilliseconds);
        fwprintf(stderr, _buffer);
        fflush(stderr);
    } else {
        OutputDebugStringW(_buffer);
    }
}

#define LOG if (g_DebugMode) DbgPrint

class tracer_t
{
public:
    const wchar_t* _name;
    tracer_t(const wchar_t* name) : _name(name) { 
        LOG(L">>> %s\n", name);
    }
    ~tracer_t() {
        LOG(L"<<< %s\n", _name);
    }
};

//#define TRACER(s) tracer_t tracer(s);
#define TRACER(s)

class ref_count_t
{
private:
    int _count;

public:
    ref_count_t() : _count(0) {}
    void add_ref() {
        _count++;
    }
    int release() {
        return --_count;
    }
};

class kfs_mapping_ptr
{
private:
    kfs_mapping_t* _p;
    ref_count_t* _rc;

public:
    kfs_mapping_ptr(kfs_mapping_t* p=NULL) : _p(p) {
        _rc = new ref_count_t();
        _rc->add_ref();
    }
    inline kfs_mapping_t* p() const { return _p; }
    inline ref_count_t* rc() const { return _rc; }
    // copy constructor
    kfs_mapping_ptr(const kfs_mapping_ptr& kp) : _p(kp.p()), _rc(kp.rc()) {
        _rc->add_ref();
    }
    ~kfs_mapping_ptr() {
        if (_rc->release() == 0) {
            LOG(L"[kfs]: deallocating memory\n");
            if (_p) delete _p;
            delete _rc;
        }
    }
    kfs_mapping_t& operator*() { return *_p; }
    kfs_mapping_t* operator->() { return _p; }
    // assignment
    kfs_mapping_ptr& operator=(const kfs_mapping_ptr& kp) {
        TRACER(L"kfs_mapping_ptr.operator=");
        if (this != &kp) {
            if (_rc->release() == 0) {
                LOG(L"[kfs]: deallocating memory on assignment\n");
                if (_p) delete _p;
                delete _rc;
            }
            _p = kp.p();
            _rc = kp.rc();
            _rc->add_ref();
        }
        return *this;
    }
    // comparisons
    template<typename T> bool operator==(T x) {
        return (void*)_p == (void*)x;
    }
    template<typename T> bool operator!=(T x) {
        return (void*)_p != (void*)x;
    }
};

// map: afs file handle --> kfs mapping
typedef hash_map<wstring, kfs_mapping_ptr> name_map_t;
typedef name_map_t::iterator name_map_iter;

class kfs_t {
public:
    config_t* _config;
    name_map_t _name_map;

    kfs_t(config_t* const p_config) : _config(p_config) {}
};

kfs_mapping_t* CreateKfsMapping(config_t* p_config, const wchar_t* filename);
kfs_directory_t* CreateKfsDirectoryInfo(
    config_t* p_config,
    const wchar_t* filename, bool up_to_date=false);
int FindFirstBinForOffset(DWORD offset, kfs_directory_t* kd);
void GetKfsPath(
    wchar_t* file_path, size_t len, const kfs_directory_t* kd, int i);

//wchar_t _buffer[512];

static void
GetFilePath(
    config_t* p_config,
    PWCHAR  filePath,
    ULONG   numberOfElements,
    LPCWSTR FileName)
{
    const wchar_t* RootDirectory = p_config->_afs_source.c_str();
    RtlZeroMemory(filePath, numberOfElements * sizeof(WCHAR));
    wcsncpy_s(filePath, numberOfElements, RootDirectory, wcslen(RootDirectory));
    if (FileName[0]!=L'\\') {
        wcsncat_s(filePath, numberOfElements, L"\\", 1);
    }
    wcsncat_s(filePath, numberOfElements, FileName, wcslen(FileName));
}


static void
PrintUserName(PDOKAN_FILE_INFO  DokanFileInfo)
{
    HANDLE  handle;
    UCHAR buffer[1024];
    DWORD returnLength;
    WCHAR accountName[256];
    WCHAR domainName[256];
    DWORD accountLength = sizeof(accountName) / sizeof(WCHAR);
    DWORD domainLength = sizeof(domainName) / sizeof(WCHAR);
    PTOKEN_USER tokenUser;
    SID_NAME_USE snu;

    handle = DokanOpenRequestorToken(DokanFileInfo);
    if (handle == INVALID_HANDLE_VALUE) {
        LOG(L"  DokanOpenRequestorToken failed\n");
        return;
    }

    if (!GetTokenInformation(handle, TokenUser, buffer, sizeof(buffer), &returnLength)) {
        LOG(L"  GetTokenInformaiton failed: %d\n", GetLastError());
        CloseHandle(handle);
        return;
    }

    CloseHandle(handle);

    tokenUser = (PTOKEN_USER)buffer;
    if (!LookupAccountSid(NULL, tokenUser->User.Sid, accountName,
            &accountLength, domainName, &domainLength, &snu)) {
        LOG(L"  LookupAccountSid failed: %d\n", GetLastError());
        return;
    }

    LOG(L"  AccountName: %s, DomainName: %s\n", accountName, domainName);
}

#define KfsCheckFlag(val, flag) if (val&flag) { LOG(L"\t" L#flag L"\n"); }

static int WINAPI
KfsCreateFile(
    LPCWSTR                 FileName,
    DWORD                   AccessMode,
    DWORD                   ShareMode,
    DWORD                   CreationDisposition,
    DWORD                   FlagsAndAttributes,
    PDOKAN_FILE_INFO        DokanFileInfo)
{
    TRACER(L"KfsCreateFile");
    WCHAR filePath[MAX_PATH];
    HANDLE handle;
    DWORD fileAttr;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"CreateFile : %s\n", filePath);


    //PrintUserName(DokanFileInfo);

    if (CreationDisposition == CREATE_NEW)
        LOG(L"\tCREATE_NEW\n");
    if (CreationDisposition == OPEN_ALWAYS)
        LOG(L"\tOPEN_ALWAYS\n");
    if (CreationDisposition == CREATE_ALWAYS)
        LOG(L"\tCREATE_ALWAYS\n");
    if (CreationDisposition == OPEN_EXISTING)
        LOG(L"\tOPEN_EXISTING\n");
    if (CreationDisposition == TRUNCATE_EXISTING)
        LOG(L"\tTRUNCATE_EXISTING\n");

    // Deny writes
    //if ((CreationDisposition == CREATE_NEW) || (
    //        CreationDisposition == CREATE_ALWAYS) || (
    //        CreationDisposition == TRUNCATE_EXISTING)) {
    //    LOG(L"CreateFile: write access is denied.\n");
    //    return -1 * ERROR_WRITE_PROTECT;
    //}

    /*
    if (ShareMode == 0 && AccessMode & FILE_WRITE_DATA)
        ShareMode = FILE_SHARE_WRITE;
    else if (ShareMode == 0)
        ShareMode = FILE_SHARE_READ;
    */

    LOG(L"\tShareMode = 0x%x\n", ShareMode);

    KfsCheckFlag(ShareMode, FILE_SHARE_READ);
    KfsCheckFlag(ShareMode, FILE_SHARE_WRITE);
    KfsCheckFlag(ShareMode, FILE_SHARE_DELETE);

    LOG(L"\tAccessMode = 0x%x\n", AccessMode);

    KfsCheckFlag(AccessMode, GENERIC_READ);
    KfsCheckFlag(AccessMode, GENERIC_WRITE);
    KfsCheckFlag(AccessMode, GENERIC_EXECUTE);
    
    KfsCheckFlag(AccessMode, DELETE);
    KfsCheckFlag(AccessMode, FILE_READ_DATA);
    KfsCheckFlag(AccessMode, FILE_READ_ATTRIBUTES);
    KfsCheckFlag(AccessMode, FILE_READ_EA);
    KfsCheckFlag(AccessMode, READ_CONTROL);
    KfsCheckFlag(AccessMode, FILE_WRITE_DATA);
    KfsCheckFlag(AccessMode, FILE_WRITE_ATTRIBUTES);
    KfsCheckFlag(AccessMode, FILE_WRITE_EA);
    KfsCheckFlag(AccessMode, FILE_APPEND_DATA);
    KfsCheckFlag(AccessMode, WRITE_DAC);
    KfsCheckFlag(AccessMode, WRITE_OWNER);
    KfsCheckFlag(AccessMode, SYNCHRONIZE);
    KfsCheckFlag(AccessMode, FILE_EXECUTE);
    KfsCheckFlag(AccessMode, STANDARD_RIGHTS_READ);
    KfsCheckFlag(AccessMode, STANDARD_RIGHTS_WRITE);
    KfsCheckFlag(AccessMode, STANDARD_RIGHTS_EXECUTE);

    /*
    if (AccessMode & (GENERIC_WRITE | DELETE
                | FILE_WRITE_DATA | FILE_WRITE_ATTRIBUTES 
                | FILE_WRITE_EA | FILE_APPEND_DATA 
                | WRITE_DAC | WRITE_OWNER)) {
        LOG(L"CreateFile : generic write/delete access is denied.\n");
        return -1 * ERROR_WRITE_PROTECT;
    }
    */

    // When filePath is a directory, needs to change the flag so that the file can be opened.
    fileAttr = GetFileAttributes(filePath);
    if (fileAttr && fileAttr & FILE_ATTRIBUTE_DIRECTORY) {
        FlagsAndAttributes |= FILE_FLAG_BACKUP_SEMANTICS;
        //AccessMode = 0;
    }
    LOG(L"\tFlagsAndAttributes = 0x%x\n", FlagsAndAttributes);

    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_ARCHIVE);
    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_ENCRYPTED);
    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_HIDDEN);
    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_NORMAL);
    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_NOT_CONTENT_INDEXED);
    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_OFFLINE);
    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_READONLY);
    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_SYSTEM);
    KfsCheckFlag(FlagsAndAttributes, FILE_ATTRIBUTE_TEMPORARY);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_WRITE_THROUGH);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_OVERLAPPED);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_NO_BUFFERING);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_RANDOM_ACCESS);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_SEQUENTIAL_SCAN);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_DELETE_ON_CLOSE);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_BACKUP_SEMANTICS);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_POSIX_SEMANTICS);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_OPEN_REPARSE_POINT);
    KfsCheckFlag(FlagsAndAttributes, FILE_FLAG_OPEN_NO_RECALL);
    KfsCheckFlag(FlagsAndAttributes, SECURITY_ANONYMOUS);
    KfsCheckFlag(FlagsAndAttributes, SECURITY_IDENTIFICATION);
    KfsCheckFlag(FlagsAndAttributes, SECURITY_IMPERSONATION);
    KfsCheckFlag(FlagsAndAttributes, SECURITY_DELEGATION);
    KfsCheckFlag(FlagsAndAttributes, SECURITY_CONTEXT_TRACKING);
    KfsCheckFlag(FlagsAndAttributes, SECURITY_EFFECTIVE_ONLY);
    KfsCheckFlag(FlagsAndAttributes, SECURITY_SQOS_PRESENT);

    // kfs: create mapping object, if does not exist
    {
        lock_t lock(&cs);
        name_map_t::iterator it = kfs->_name_map.find(FileName);
        if (it == kfs->_name_map.end()) {
            kfs_mapping_t* km = CreateKfsMapping(kfs->_config, FileName);
            kfs_mapping_ptr kp(km);
            kfs->_name_map[FileName] = kp;
        }
    }

    handle = CreateFile(
        filePath,
        AccessMode,//GENERIC_READ|GENERIC_WRITE|GENERIC_EXECUTE,
        ShareMode,
        NULL, // security attribute
        CreationDisposition,
        FlagsAndAttributes,// |FILE_FLAG_NO_BUFFERING,
        NULL); // template file handle

    if (handle == INVALID_HANDLE_VALUE) {
        DWORD error = GetLastError();
        LOG(L"\terror code = %d\n\n", error);
        return error * -1; // error codes are negated value of Windows System Error codes
    }

    LOG(L"\n");

    // save the file handle in Context
    DokanFileInfo->Context = (ULONG64)handle;
    return 0;
}


static int WINAPI
KfsCreateDirectory(
    LPCWSTR                 FileName,
    PDOKAN_FILE_INFO        DokanFileInfo)
{
    TRACER(L"KfsCreateDirectory");
    WCHAR filePath[MAX_PATH];
    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"CreateDirectory : %s\n", filePath);
    if (!CreateDirectory(filePath, NULL)) {
        DWORD error = GetLastError();
        LOG(L"\terror code = %d\n\n", error);
        return error * -1; // error codes are negated value of Windows System Error codes
    }
    return 0;
}


static int WINAPI
KfsOpenDirectory(
    LPCWSTR                 FileName,
    PDOKAN_FILE_INFO        DokanFileInfo)
{
    TRACER(L"KfsOpenDirectory");
    WCHAR filePath[MAX_PATH];
    HANDLE handle;
    DWORD attr;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"OpenDirectory : %s\n", filePath);

    attr = GetFileAttributes(filePath);
    if (attr == INVALID_FILE_ATTRIBUTES) {
        DWORD error = GetLastError();
        LOG(L"\terror code = %d\n\n", error);
        return error * -1;
    }
    if (!(attr & FILE_ATTRIBUTE_DIRECTORY)) {
        return -1;
    }

    handle = CreateFile(
        filePath,
        0,
        FILE_SHARE_READ|FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        FILE_FLAG_BACKUP_SEMANTICS,
        NULL);

    if (handle == INVALID_HANDLE_VALUE) {
        DWORD error = GetLastError();
        LOG(L"\terror code = %d\n\n", error);
        return error * -1;
    }

    LOG(L"\n");

    DokanFileInfo->Context = (ULONG64)handle;

    return 0;
}


static int WINAPI
KfsCloseFile(
    LPCWSTR                 FileName,
    PDOKAN_FILE_INFO        DokanFileInfo)
{
    TRACER(L"KfsCloseFile");
    WCHAR filePath[MAX_PATH];
    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    if (DokanFileInfo->Context) {
        LOG(L"CloseFile: %s\n", filePath);
        LOG(L"\terror : not cleanuped file\n\n");
        CloseHandle((HANDLE)DokanFileInfo->Context);
        DokanFileInfo->Context = 0;
    } else {
        //LOG(L"Close: %s\n\tinvalid handle\n\n", filePath);
        LOG(L"Close: %s\n\n", filePath);
        return 0;
    }

    //LOG(L"\n");
    return 0;
}


static int WINAPI
KfsCleanup(
    LPCWSTR                 FileName,
    PDOKAN_FILE_INFO        DokanFileInfo)
{
    TRACER(L"KfsCleanup");
    WCHAR filePath[MAX_PATH];
    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    if (DokanFileInfo->Context) {
        LOG(L"Cleanup: %s\n\n", filePath);

        // clean-up kfs mapping structure
        {
            lock_t lock(&cs);
            name_map_t::iterator it = kfs->_name_map.find(FileName);
            if (it != kfs->_name_map.end()) {
                if (it->second != NULL && !it->second->is_file) {
                    // directory
                    kfs_directory_t* kd = (kfs_directory_t*)it->second.p();
                    kd->up_to_date = false;
                }
                else if (it->second != NULL) {
                    /// file
                    kfs_mapping_t* km = it->second.p();
                    kfs->_name_map.erase(it);
                }
            }
        }
        
        CloseHandle((HANDLE)DokanFileInfo->Context);
        DokanFileInfo->Context = 0;

        if (DokanFileInfo->DeleteOnClose) {
            LOG(L"\tDeleteOnClose\n");
            if (DokanFileInfo->IsDirectory) {
                LOG(L"  DeleteDirectory ");
                if (!RemoveDirectory(filePath)) {
                    LOG(L"error code = %d\n\n", GetLastError());
                } else {
                    LOG(L"success\n\n");
                }
            } else {
                LOG(L"  DeleteFile ");
                if (DeleteFile(filePath) == 0) {
                    LOG(L" error code = %d\n\n", GetLastError());
                } else {
                    LOG(L"success\n\n");
                }
            }
        }

    } else {
        LOG(L"Cleanup: %s\n\tinvalid handle\n\n", filePath);
        return -1;
    }

    LOG(L"cleanup done.\n");
    return 0;
}

int FindFirstBinForOffset(DWORD offset, kfs_directory_t* kd)
{
    TRACER(L"FindFirstBinForOffset");
    LOG(L"[kfs]: FindFirstBinForOffset: looking for offset 0x%x\n", offset);
    int left = 0;
    int right = 0xffff;
    if (offset >= kd->size.LowPart) {
        return -1;
    }
    while (true) {
        int i = (left + right)/2;
        DWORD a = kd->entries[i].afs_offset;
        DWORD b = a + ((kd->entries[i].file_size_aligned != 0) 
            ? kd->entries[i].file_size_aligned 
            : kd->entries[i].afs_original_size);

        if (a <= offset && offset < b) {
            LOG(L"[kfs]: FindFirstBinForOffset: found %d\n", i);
            return i;
        }
        else if (offset < a) {
            right = i;
            // check for not found
            if (left == right) break;
        }
        else {
            left = ((left == i) ? (i + 1) : i);
            // check for not found
            if (left == right) break;
        }
        //LOG(L"[kfs]: left=%d, right=%d\n", left, right);
    }
    return -1;
}

static int WINAPI
KfsReadFile(
    LPCWSTR             FileName,
    LPVOID              Buffer,
    DWORD               BufferLength,
    LPDWORD             ReadLength,
    LONGLONG            Offset,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsReadFile");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle = (HANDLE)DokanFileInfo->Context;
    ULONG   offset = (ULONG)Offset;
    BOOL    opened = FALSE;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"ReadFile : %s (offset=0x%x, BufferLength=0x%x\n", 
        filePath, offset, BufferLength);

    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle, cleanuped?\n");
        handle = CreateFile(
            filePath,
            GENERIC_READ,
            FILE_SHARE_READ,
            NULL,
            OPEN_EXISTING,
            0,
            NULL);
        if (handle == INVALID_HANDLE_VALUE) {
            LOG(L"\tCreateFile error : %d\n\n", GetLastError());
            return -1;
        }
        opened = TRUE;
    }
    
    // check name map
    kfs_mapping_t* km = NULL;
    {
        lock_t lock(&cs);
        name_map_t::iterator it = kfs->_name_map.find(FileName);
        if (it == kfs->_name_map.end()) {
            km = CreateKfsMapping(kfs->_config, FileName);
            kfs_mapping_ptr kp(km);
            kfs->_name_map[FileName] = kp;
        }
        else {
            km = it->second.p();
        }
    }

    if (km && km->is_file) {
        // it's a file. Read appropriate portion of it
        HANDLE kfs_handle = CreateFile(
            km->path.c_str(),
            GENERIC_READ,
            FILE_SHARE_READ,
            NULL,
            OPEN_EXISTING,
            0,
            NULL);
        if (kfs_handle == INVALID_HANDLE_VALUE) {
            LOG(L"[kfs]: WARN tried to read from kfs file: {%s}. "
                L"error code: %d\n", km->path.c_str(), GetLastError());
            return -1;
        }

        if (SetFilePointer(kfs_handle, 
                offset, NULL, FILE_BEGIN) == 0xFFFFFFFF) {
            LOG(L"\tseek error, offset = %d\n\n", offset);
            CloseHandle(kfs_handle);
            return -1;
        }

        if (!ReadFile(kfs_handle, Buffer, BufferLength, ReadLength, NULL)) {
            LOG(L"\tread error = %u, buffer length = %d, "
                     L"read length = %d\n\n",
                     GetLastError(), BufferLength, *ReadLength);
            CloseHandle(kfs_handle);
            return -1;
        }

        CloseHandle(kfs_handle);
        return 0;
    }

    // check if it is a directory and needs to update
    kfs_directory_t* kd = NULL;
    if (km && !km->is_file) {
        kd = (kfs_directory_t*)km;

        // check if AFS header and tos are being read
        
        BYTE* b = (BYTE*)Buffer;

        size_t h_from = min(sizeof(kfs_afs_header_t), offset);
        size_t h_to = min(sizeof(kfs_afs_header_t), offset + BufferLength);
        if (h_to > h_from) {
            // update kfs directory object
            if (!kd->up_to_date) {
                LOG(L"up_to_date=%d\n", kd->up_to_date);
                lock_t lock(&cs);
                kd = CreateKfsDirectoryInfo(kfs->_config, FileName, true);
                kfs->_name_map[FileName] = kd;
            }
        }
    }

    if (kd) {
        LOG(L"[kfs]: found kfs directory! (size=%d)\n", 
            kd->size.LowPart);

        // read data from kfs_directory

        // a read can potentially span 2 things:
        // 1. a portion (or whole) of AFS header and tos
        
        BYTE* b = (BYTE*)Buffer;

        size_t h_from = min(sizeof(kfs_afs_header_t), offset);
        size_t h_to = min(sizeof(kfs_afs_header_t), offset + BufferLength);
        if (h_to > h_from) {
            // AFS header and tos
            BYTE* p = kd->header_and_tos.bytes + h_from;
            memcpy(b, p, h_to - h_from);
            //LOG(L"[kfs]: BufferLength: 0x%x bytes, offset=0x%x\n",
            //    BufferLength, offset);
            LOG(L"[kfs]: copied 0x%x bytes of AFS header-and-tos. "
                L"offset=0x%x\n", h_to - h_from, h_from);
        }

        // 2. fragments of one or more BIN files
        DWORD bytes_read = h_to - h_from;
        DWORD bytes_to_read = BufferLength - bytes_read;
        b += bytes_read;
        LOG(L"[kfs]: bytes_to_read=0x%x\n", bytes_to_read);

        if (bytes_to_read > 0) {
            LOG(L"[kfs]: reading beyond header_and_tos!\n");
            DWORD curr_offset = offset + bytes_read;

            // find first BIN
            int i = FindFirstBinForOffset(curr_offset, kd);
            LOG(L"[kfs]: offset=0x%x, i=%d\n", curr_offset, i);
            if (i==-1) {
                LOG(L"[kfs]: ERROR: couldn't find BIN\n");
                if (opened)
                    CloseHandle(handle);
                return -1;
            }

            while (bytes_to_read > 0) {
                if (kd->entries[i].file_size) {
                    // a file exists in kfs directory
                    DWORD local_offset = 
                        curr_offset - kd->entries[i].afs_offset;
                    DWORD bytes_to_read_from_file = min(
                        kd->entries[i].file_size_aligned - local_offset,
                        bytes_to_read);
                    DWORD bytes_read_from_file = 0;

                    wchar_t file_path[MAX_PATH];
                    GetKfsPath(
                        file_path, MAX_PATH, kd, i);
                    LOG(L"[kfs]: opening {%s}\n", file_path);

                    HANDLE temp_handle = CreateFile(
                        file_path,
                        GENERIC_READ,
                        FILE_SHARE_READ,
                        NULL,
                        OPEN_EXISTING,
                        0,
                        NULL);
                    if (temp_handle == INVALID_HANDLE_VALUE) {
                        LOG(L"[kfs]: ERROR: could not open file: {%s}\n",
                            file_path);
                        if (opened)
                            CloseHandle(handle);
                        return -1;
                    }
                    LOG(L"[kfs]: setting file pointer to 0x%x\n", 
                        local_offset);
                    if (SetFilePointer(temp_handle, local_offset, 
                                       NULL, FILE_BEGIN) == -1) {
                        LOG(L"\tseek error, offset = %d\n\n", 
                            local_offset);
                        CloseHandle(temp_handle);
                        if (opened)
                            CloseHandle(handle);
                        return -1;
                    }
                    LOG(L"[kfs]: reading 0x%x bytes ...\n",
                        bytes_to_read_from_file);
                    if (!ReadFile(
                            temp_handle, b, bytes_to_read_from_file,
                            &bytes_read_from_file, NULL)) {
                        CloseHandle(temp_handle);
                        if (opened)
                            CloseHandle(handle);
                        return -1;
                    }
                    LOG(L"[kfs]: successfully read 0x%x bytes.\n",
                        bytes_read_from_file);
                    CloseHandle(temp_handle);
                    b += bytes_read_from_file;

                    // pad with zeroes
                    size_t unread = bytes_to_read_from_file -
                        bytes_read_from_file;
                    if (unread) {
                        RtlZeroMemory(b, unread);
                        b += unread;
                    }

                    bytes_read += bytes_to_read_from_file;
                    bytes_to_read -= bytes_to_read_from_file;
                    curr_offset += bytes_to_read_from_file;
                }
                else {
                    // read from real AFS file
                    size_t afs_original_size = PAGED(
                        kd->entries[i].afs_original_size);
                    DWORD bytes_to_read_from_afs = min(
                        afs_original_size, bytes_to_read);
                    DWORD bytes_read_from_afs = 0;
                    DWORD afs_offset = kd->entries[i].afs_original_offset +
                        curr_offset - kd->entries[i].afs_offset;
                    if (SetFilePointer(
                        handle, afs_offset, NULL, FILE_BEGIN) == -1) {
                        LOG(L"\tseek error, offset = %d\n\n", afs_offset);
                        if (opened)
                            CloseHandle(handle);
                        return -1;
                    }
                    if (!ReadFile(
                            handle, b, bytes_to_read_from_afs, 
                            &bytes_read_from_afs, NULL)) {
                        LOG(L"\tread error = %u, buffer length = %d, "
                                 L"read length = %d\n\n",
                                 GetLastError(), BufferLength, *ReadLength);
                        if (opened)
                            CloseHandle(handle);
                        return -1;

                    } else {
                        LOG(L"\t[kfs]: read 0x%x bytes, offset 0x%x\n\n", 
                            bytes_read_from_afs, offset);
                    }

                    bytes_read += bytes_read_from_afs;
                    bytes_to_read -= bytes_read_from_afs;
                    b += bytes_read_from_afs;
                    curr_offset += bytes_read_from_afs;
                }
                i++;
                LOG(L"[kfs]: --> next: i=%d, bytes_to_read=0x%x\n",
                    i, bytes_to_read);

                if (curr_offset >= kd->size.LowPart) {
                    LOG(L"[kfs]: reached the end\n");
                    break;
                }
            }
        }
        *ReadLength = bytes_read;
    }
    else {
        // no kfs directory
        if (SetFilePointer(handle, offset, NULL, FILE_BEGIN) == 0xFFFFFFFF) {
            LOG(L"\tseek error, offset = %d\n\n", offset);
            if (opened)
                CloseHandle(handle);
            return -1;
        }

        if (!ReadFile(handle, Buffer, BufferLength, ReadLength, NULL)) {
            LOG(L"\tread error = %u, buffer length = %d, "
                     L"read length = %d\n\n",
                     GetLastError(), BufferLength, *ReadLength);
            if (opened)
                CloseHandle(handle);
            return -1;

        } else {
            //LOG(L"\tread %d, offset %d\n\n", *ReadLength, offset);
        }
    }

    LOG(L"[kfs]: TOTAL-READ 0x%x bytes from offset=0x%x\n", 
        *ReadLength, offset);

    if (opened)
        CloseHandle(handle);

    return 0;
}


static int WINAPI
KfsWriteFile(
    LPCWSTR     FileName,
    LPCVOID     Buffer,
    DWORD       NumberOfBytesToWrite,
    LPDWORD     NumberOfBytesWritten,
    LONGLONG            Offset,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsWriteFile");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle = (HANDLE)DokanFileInfo->Context;
    ULONG   offset = (ULONG)Offset;
    BOOL    opened = FALSE;

    //LOG(L"WriteFile : ERROR: writes are not supported");
    //if (NumberOfBytesWritten) {
    //    *NumberOfBytesWritten = 0;
    //}
    //return -1 * ERROR_WRITE_PROTECT;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"WriteFile : %s, offset %I64d, length %d\n", filePath, Offset, NumberOfBytesToWrite);

    // reopen the file
    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle, cleanuped?\n");
        handle = CreateFile(
            filePath,
            GENERIC_WRITE,
            FILE_SHARE_WRITE,
            NULL,
            OPEN_EXISTING,
            0,
            NULL);
        if (handle == INVALID_HANDLE_VALUE) {
            LOG(L"\tCreateFile error : %d\n\n", GetLastError());
            return -1;
        }
        opened = TRUE;
    }

    if (DokanFileInfo->WriteToEndOfFile) {
        if (SetFilePointer(handle, 0, NULL, FILE_END) == INVALID_SET_FILE_POINTER) {
            LOG(L"\tseek error, offset = EOF, error = %d\n", GetLastError());
            return -1;
        }
    } else if (SetFilePointer(handle, offset, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER) {
        LOG(L"\tseek error, offset = %d, error = %d\n", offset, GetLastError());
        return -1;
    }

        
    if (!WriteFile(handle, Buffer, NumberOfBytesToWrite, NumberOfBytesWritten, NULL)) {
        LOG(L"\twrite error = %u, buffer length = %d, write length = %d\n",
            GetLastError(), NumberOfBytesToWrite, *NumberOfBytesWritten);
        return -1;

    } else {
        LOG(L"\twrite %d, offset %d\n\n", *NumberOfBytesWritten, offset);
    }

    // close the file when it is reopened
    if (opened)
        CloseHandle(handle);

    return 0;
}


static int WINAPI
KfsFlushFileBuffers(
    LPCWSTR     FileName,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsFlushFileBuffers");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle = (HANDLE)DokanFileInfo->Context;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"FlushFileBuffers : %s\n", filePath);

    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");
        return 0;
    }

    if (FlushFileBuffers(handle)) {
        return 0;
    } else {
        LOG(L"\tflush error code = %d\n", GetLastError());
        return -1;
    }

}

void GetKfsFilePath(
    wchar_t* path, size_t len, const wchar_t* filename,
    const list<wstring>::iterator kfs_roots_iter)
{
    wcscpy(path, kfs_roots_iter->c_str());
    if (filename[0]!=L'\\') {
        wcscat(path, L"\\");
    }
    wcscat(path, filename);
}

void GetKfsFindPattern(
    wchar_t* pattern, size_t len, const wchar_t* filename,
    const list<wstring>::iterator kfs_roots_iter)
{
    GetKfsFilePath(pattern, len, filename, kfs_roots_iter);
    wcscat(pattern, L"\\*.*");
}

void GetKfsPath(
    wchar_t* file_path, size_t len, const kfs_directory_t* kd, int i)
{
    wcscpy(file_path, kd->entries[i].kfs_root->c_str());
    if (kd->path[0]!=L'\\') {
        wcscat(file_path, L"\\");
    }
    wcscat(file_path, kd->path.c_str());
    wcscat(file_path, L"\\");
    wcscat(file_path, kd->entries[i].filename);
}

void GetAfsNames(config_t* p_config, 
        const wchar_t* filename, afs_names_t& names)
{
    wchar_t real_file_path[MAX_PATH];

    GetFilePath(p_config, real_file_path, MAX_PATH, filename);

    //FILE* f = _wfopen(real_file_path, L"rb");
    //if (!f) {
    //    LOG(L"[kfs]: ERROR: cannot open file %s\n", real_file_path);
    //    return false;
    //}

    HANDLE h = CreateFile(
        real_file_path,
        GENERIC_READ,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);
    if (h == INVALID_HANDLE_VALUE) {
        LOG(L"[kfs]: ERROR: cannot open file %s\n", real_file_path);
        return;
    }
    DWORD bytes_read = 0;

    AFSDIRHEADER afs_header;
    ReadFile(h, &afs_header, sizeof(AFSDIRHEADER), &bytes_read, NULL);

    // check if names directory exists
    AFSITEMINFO namesItemInfo;
    ZeroMemory(&namesItemInfo, sizeof(AFSITEMINFO));

    SetFilePointer(h, sizeof(AFSDIRHEADER) +
            afs_header.dwNumFiles * sizeof(AFSITEMINFO),
            NULL, FILE_BEGIN);
    ReadFile(h, &namesItemInfo, sizeof(AFSITEMINFO), &bytes_read, NULL);

    if (namesItemInfo.dwOffset) {
        // skip to names directory
        SetFilePointer(h, namesItemInfo.dwOffset, NULL, FILE_BEGIN);
        int num_names = namesItemInfo.dwSize / sizeof(AFSNAMEINFO);
        AFSNAMEINFO *nameInfo = (AFSNAMEINFO*)HeapAlloc(
                GetProcessHeap(), HEAP_ZERO_MEMORY, namesItemInfo.dwSize);
        ReadFile(h, (BYTE*)nameInfo, namesItemInfo.dwSize, &bytes_read, NULL);

        for (int i=0; i<num_names; i++) {
            wchar_t name[32];
            MultiByteToWideChar(
                CP_UTF8, 0, nameInfo[i].szFileName, 32, name, 32);
            names._names[name] = i;
            //LOG(L"[kfs]: %d: {%s}\n", i, name);
        }
        LOG(L"[kfs]: loaded %d names\n", num_names);
        HeapFree(GetProcessHeap(), 0, nameInfo);
    }

    names.loaded = true;
    CloseHandle(h);
}

kfs_mapping_t* CreateKfsMapping(config_t* p_config, const wchar_t* filename)
{
    TRACER(L"CreateKfsMapping");
    LOG(L"[kfs]: checking %s\n", filename);
    if (wcscmp(filename, L"\\")==0) {
        return NULL;
    }

    kfs_mapping_t* km = NULL;
    list<wstring>::iterator rit = p_config->_kfs_roots.begin();
    for (; rit != p_config->_kfs_roots.end(); rit++) {
        wchar_t path[MAX_PATH];
        GetKfsFilePath(path, MAX_PATH, filename, rit);

        WIN32_FIND_DATA fData;
        RtlZeroMemory(&fData, sizeof(fData));
        LOG(L"[kfs]: looking for {%s}\n", path);

        HANDLE hff = FindFirstFile(path, &fData);

        // skip return if kfs mapping not found
        if (hff == INVALID_HANDLE_VALUE) {
            continue;
        }

        // check if it's a directory
        if (fData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            return CreateKfsDirectoryInfo(p_config, filename);
        }

        km = new kfs_mapping_t();
        if (!km) {
            LOG(L"[kfs]: ERROR creating kfs_mapping_t object\n");
            return NULL;
        }

        // store inforamtion about the file
        km->path = path;
        km->size.LowPart = fData.nFileSizeLow;
        km->size.HighPart = fData.nFileSizeHigh;

        LOG(L"[kfs]: found {%s}\n", path);
    }

    return km;
}

kfs_directory_t* CreateKfsDirectoryInfo(
        config_t* p_config,
        const wchar_t* filename, bool up_to_date)
{
    TRACER(L"CreateKfsDirectoryInfo");
    LOG(L"[kfs]: checking %s\n", filename);
    if (wcscmp(filename, L"\\")==0) {
        return NULL;
    }

    afs_names_t names;

    kfs_directory_t* kd = NULL;
    list<wstring>::iterator rit = p_config->_kfs_roots.begin();
    for (; rit != p_config->_kfs_roots.end(); rit++) {
        wchar_t pattern[MAX_PATH];
        GetKfsFindPattern(pattern, MAX_PATH, filename, rit);

        WIN32_FIND_DATA fData;
        RtlZeroMemory(&fData, sizeof(fData));
        LOG(L"[kfs]: looking for {%s}\n", pattern);

        HANDLE hff = FindFirstFile(pattern, &fData);

        // skip return if kfs directory not found (or is empty)
        if (hff == INVALID_HANDLE_VALUE) {
            continue;
        }

        LOG(L"DEBUG: found something!\n");
        
        // allocate new kfs directory info object
        if (!kd) {
            kd = new kfs_directory_t();
            if (!kd) {
                LOG(L"[kfs]: ERROR: unable to create kfs directory info "
                    L"for file %s\n", filename);
                return NULL;
            }
            kd->up_to_date = up_to_date;
            kd->path = filename;
            LOG(L"up_to_date=%d\n", kd->up_to_date);
        }

        // get AFS names, if present in AFS file
        if (!names.loaded) {
            GetAfsNames(p_config, filename, names);
        }

        while (hff != INVALID_HANDLE_VALUE)
        {
            // check if this is a file
            if (!(fData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) 
            {
                int binId = -1;
                wchar_t* s = wcsrchr(fData.cFileName,'_');
                if (!(s && swscanf(s+1,L"%d",&binId)==1)) {
                    // check names directory
                    binId = names.find(fData.cFileName);
                }
                if (binId >= 0)
                {
                    LOG(L"[kfs]: found bin={%d}, file={%s}\n",
                        binId, fData.cFileName);

                    if (binId >= 0xffff)
                    {
                        // binID too large
                        LOG(L"ERROR: bin ID for filename \"%s\" "
                            L"is too large. Maximum bin ID is: %d\n", 
                            fData.cFileName, 
                            0xffff);
                    }
                    else if (wcslen(fData.cFileName) >= 
                            p_config->_fileNameLen)
                    {
                        // file name too long
                        LOG(L"ERROR: filename too long: \"%s\" "
                            L"(in folder: %s)\n", 
                            fData.cFileName, filename);
                        LOG(L"ERROR: length = %d chars. "
                            L"Maximum allowed length: %d chars.\n", 
                            wcslen(fData.cFileName), 
                            p_config->_fileNameLen-1);
                    }
                    else
                    {
                        // store information about the file
                        wcsncpy(kd->entries[binId].filename,
                            fData.cFileName, p_config->_fileNameLen-1);
                        kd->entries[binId].kfs_root = rit;
                        kd->entries[binId].file_size = fData.nFileSizeLow;
                        kd->entries[binId].file_size_aligned = PAGED(
                            fData.nFileSizeLow);
                    }
                }
            }

            // proceed to next file
            if (!FindNextFile(hff, &fData)) break;
        }
        FindClose(hff);
    }

    LOG(L"got here!\n");

    // exit now, if kfs directory object wasn't created
    if (!kd) {
        return NULL;
    }

    // build full kfs directory info

    // take into account AFS header and TOS
    LARGE_INTEGER total_size;
    total_size.LowPart = AFS_HEADER_AND_TOS_SIZE;
    total_size.HighPart = 0;

    // walk the AFS file
    wchar_t real_file_path[MAX_PATH];
    GetFilePath(p_config, real_file_path, MAX_PATH, filename);

    //FILE* f = _wfopen(real_file_path, L"rb");
    //if (!f) {
    //    LOG(L"[kfs]: ERROR: cannot open file %s\n", real_file_path);
    //    return false;
    //}

    HANDLE h = CreateFile(
        real_file_path,
        GENERIC_READ,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);
    if (h == INVALID_HANDLE_VALUE) {
        LOG(L"[kfs]: ERROR: cannot open file %s\n", real_file_path);
        return false;
    }
    DWORD bytes_read = 0;

    kd->header_and_tos.data.header.dwSig = AFSSIG;
    kd->header_and_tos.data.header.dwNumFiles = 0xffff;
    DWORD curr_offset = total_size.LowPart;

    AFSDIRHEADER afs_header;
    ReadFile(h, &afs_header, sizeof(AFSDIRHEADER), &bytes_read, NULL);
    for (int i=0; i<0x10000; i++) {
        DWORD size = 0;
        if (i<=afs_header.dwNumFiles) {
            AFSITEMINFO item_info;
            ReadFile(h, &item_info, sizeof(AFSITEMINFO), &bytes_read, NULL);
            // align size to 0x800 pages
            size = PAGED(item_info.dwSize);
            // take note of original offset
            kd->entries[i].afs_original_offset = item_info.dwOffset;
            kd->entries[i].afs_original_size = item_info.dwSize;
            kd->header_and_tos.data.tos[i].dwSize = 
                item_info.dwSize;
        }
        // override with kfs size, if exists
        if (kd->entries[i].file_size_aligned != 0) {
            size = kd->entries[i].file_size_aligned;
            kd->header_and_tos.data.tos[i].dwSize = 
                kd->entries[i].file_size;
        }

        // take note of new offset
        kd->entries[i].afs_offset = total_size.LowPart;

        // update header structure
        kd->header_and_tos.data.tos[i].dwOffset = kd->entries[i].afs_offset;
        //kd->header_and_tos.data.tos[i].dwSize = size;

        LARGE_INTEGER i_size;
        i_size.HighPart = 0;
        i_size.LowPart = size;

        total_size.QuadPart += i_size.QuadPart;
    }

    if (!p_config->_extend_afs) {
        // keep the number of files in the AFS as specified.
        kd->header_and_tos.data.header.dwNumFiles = afs_header.dwNumFiles;
    }

    CloseHandle(h);

    kd->size.QuadPart = total_size.QuadPart;
    LOG(L"[kfs]: total_size now: %d\n", total_size.LowPart);

    //FILE *of = _wfopen(L".\\z.afs", L"wb");
    //fwrite(&kd->header_and_tos, sizeof(kfs_afs_header_t), 1, of);
    //fclose(of);

    return kd;
}

bool GetAfsFileSizeFromDir(const kfs_directory_t* kd, LARGE_INTEGER& fs)
{
    if (!kd) {
        return false;
    }
    fs.QuadPart = kd->size.QuadPart;
    return true;
}

static int WINAPI
KfsGetFileInformation(
    LPCWSTR                         FileName,
    LPBY_HANDLE_FILE_INFORMATION    HandleFileInformation,
    PDOKAN_FILE_INFO                DokanFileInfo)
{
    TRACER(L"KfsGetFileInformation");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle = (HANDLE)DokanFileInfo->Context;
    BOOL    opened = FALSE;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"GetFileInfo : %s\n", filePath);

    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");

        // If CreateDirectory returned FILE_ALREADY_EXISTS and 
        // it is called with FILE_OPEN_IF, that handle must be opened.
        handle = CreateFile(filePath, 0, FILE_SHARE_READ, NULL, OPEN_EXISTING,
            FILE_FLAG_BACKUP_SEMANTICS, NULL);
        if (handle == INVALID_HANDLE_VALUE)
            return -1;
        opened = TRUE;
    }

    if (!GetFileInformationByHandle(handle,HandleFileInformation)) {
        LOG(L"\terror code = %d\n", GetLastError());

        // FileName is a root directory
        // in this case, FindFirstFile can't get directory information
        if (wcslen(FileName) == 1) {
            LOG(L"  root dir\n");
            HandleFileInformation->dwFileAttributes = GetFileAttributes(filePath);

        } else {
            WIN32_FIND_DATAW find;
            RtlZeroMemory(&find, sizeof(WIN32_FIND_DATAW));
            handle = FindFirstFile(filePath, &find);
            if (handle == INVALID_HANDLE_VALUE) {
                LOG(L"\tFindFirstFile error code = %d\n\n", GetLastError());
                return -1;
            }
            HandleFileInformation->dwFileAttributes = find.dwFileAttributes;
            HandleFileInformation->ftCreationTime = find.ftCreationTime;
            HandleFileInformation->ftLastAccessTime = find.ftLastAccessTime;
            HandleFileInformation->ftLastWriteTime = find.ftLastWriteTime;
            HandleFileInformation->nFileSizeHigh = find.nFileSizeHigh;
            HandleFileInformation->nFileSizeLow = find.nFileSizeLow;

            LOG(L"\tFindFiles OK, file size = %d\n", find.nFileSizeLow);
            FindClose(handle);
        }
    } else {
        LOG(L"\tGetFileInformationByHandle success, file size = %d\n",
            HandleFileInformation->nFileSizeLow);
    }

    // lookup kfs directory object
    kfs_mapping_t* km = NULL;
    {
        lock_t lock(&cs);
        name_map_t::iterator it = kfs->_name_map.find(FileName);
        if (it != kfs->_name_map.end()) {
            km = it->second.p();
            if (!km || km->is_file) {
                kfs->_name_map.erase(it);
                km = CreateKfsMapping(kfs->_config, FileName);
                kfs_mapping_ptr kp(km);
                kfs->_name_map[FileName] = kp;
            }
            else if (km && !km->is_file) {
                kfs_directory_t* kd = (kfs_directory_t*)km;
                if (!kd->up_to_date) {
                    LOG(L"found kd, but kd->up_to_date=%d\n", kd->up_to_date);
                    kd = CreateKfsDirectoryInfo(kfs->_config, FileName, true);
                    kfs_mapping_ptr kp(kd);
                    kfs->_name_map[FileName] = kp;
                    km = kd;
                }
            }
        }
    }

    // adjust file size information, if found a mapping
    if (km) {
        HandleFileInformation->nFileSizeHigh = km->size.HighPart;
        HandleFileInformation->nFileSizeLow = km->size.LowPart;

        LOG(L"[kfs]: nFileSizeLow: %d\n", 
            HandleFileInformation->nFileSizeLow);
        LOG(L"[kfs]: nFileSizeHigh: %d\n", 
            HandleFileInformation->nFileSizeHigh);
    }

    LOG(L"\n");

    if (opened) {
        CloseHandle(handle);
    }

    return 0;
}

static void AdjustKfsFileSize(kfs_t* kfs, WIN32_FIND_DATAW& findData)
{
    wchar_t filename[MAX_PATH];
    wcscpy(filename, L"\\");
    wcscat(filename, findData.cFileName);

    if (filename[1] == L'\0') return;
    if (wcscmp(filename, L"\\.")==0) return;
    if (wcscmp(filename, L"\\..")==0) return;

    // kfs: create directory object, if does not exist
    kfs_mapping_t* km = NULL;
    {
        lock_t lock(&cs);
        name_map_t::iterator it = kfs->_name_map.find(filename);
        if (it == kfs->_name_map.end()) {
            km = CreateKfsMapping(kfs->_config, filename);
            kfs_mapping_ptr kp(km);
            kfs->_name_map[filename] = kp;
        }
        else {
            km = it->second.p();
        }
    }

    if (km) {
        findData.nFileSizeHigh = km->size.HighPart;
        findData.nFileSizeLow = km->size.LowPart;
    }
}

static int WINAPI
KfsFindFiles(
    LPCWSTR             FileName,
    PFillFindData       FillFindData, // function pointer
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsFindFiles");
    WCHAR               filePath[MAX_PATH];
    HANDLE              hFind;
    WIN32_FIND_DATAW    findData;
    DWORD               error;
    PWCHAR              yenStar = L"\\*";
    int count = 0;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    wcscat_s(filePath, MAX_PATH, yenStar);
    LOG(L"FindFiles :%s\n", filePath);

    hFind = FindFirstFile(filePath, &findData);

    if (hFind == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid file handle. Error is %u\n\n", GetLastError());
        return -1;
    }

    LOG(L"findData.cFileName = {%s}\n", findData.cFileName);
    AdjustKfsFileSize(kfs, findData);
    FillFindData(&findData, DokanFileInfo);
    count++;

    while (FindNextFile(hFind, &findData) != 0) {
        LOG(L"findData.cFileName = {%s}\n", findData.cFileName);
        AdjustKfsFileSize(kfs, findData);
        FillFindData(&findData, DokanFileInfo);
        count++;
    }
    
    error = GetLastError();
    FindClose(hFind);

    if (error != ERROR_NO_MORE_FILES) {
        LOG(L"\tFindNextFile error. Error is %u\n\n", error);
        return -1;
    }

    LOG(L"\tFindFiles return %d entries in %s\n\n", count, filePath);

    return 0;
}


static int WINAPI
KfsDeleteFile(
    LPCWSTR             FileName,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsDeleteFile");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle = (HANDLE)DokanFileInfo->Context;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"DeleteFile %s\n", filePath);

    return 0;
}


static int WINAPI
KfsDeleteDirectory(
    LPCWSTR             FileName,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsDeleteDirectory");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle = (HANDLE)DokanFileInfo->Context;
    HANDLE  hFind;
    WIN32_FIND_DATAW    findData;
    ULONG   fileLen;

    RtlZeroMemory(filePath, sizeof(filePath));
    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"DeleteDirectory %s\n", filePath);

    fileLen = wcslen(filePath);
    if (filePath[fileLen-1] != L'\\') {
        filePath[fileLen++] = L'\\';
    }
    filePath[fileLen] = L'*';

    hFind = FindFirstFile(filePath, &findData);
    while (hFind != INVALID_HANDLE_VALUE) {
        if (wcscmp(findData.cFileName, L"..") != 0 &&
            wcscmp(findData.cFileName, L".") != 0) {
            FindClose(hFind);
            LOG(L"  Directory is not empty: %s\n", findData.cFileName);
            return -(int)ERROR_DIR_NOT_EMPTY;
        }
        if (!FindNextFile(hFind, &findData)) {
            break;
        }
    }
    FindClose(hFind);

    if (GetLastError() == ERROR_NO_MORE_FILES) {
        return 0;
    } else {
        return -1;
    }
}


static int WINAPI
KfsMoveFile(
    LPCWSTR             FileName, // existing file name
    LPCWSTR             NewFileName,
    BOOL                ReplaceIfExisting,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsMoveFile");
    WCHAR           filePath[MAX_PATH];
    WCHAR           newFilePath[MAX_PATH];
    BOOL            status;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);
    GetFilePath(kfs->_config, newFilePath, MAX_PATH, NewFileName);

    LOG(L"MoveFile %s -> %s\n\n", filePath, newFilePath);

    if (DokanFileInfo->Context) {
        // should close? or rename at closing?
        CloseHandle((HANDLE)DokanFileInfo->Context);
        DokanFileInfo->Context = 0;
    }

    if (ReplaceIfExisting)
        status = MoveFileEx(filePath, newFilePath, MOVEFILE_REPLACE_EXISTING);
    else
        status = MoveFile(filePath, newFilePath);

    if (status == FALSE) {
        DWORD error = GetLastError();
        LOG(L"\tMoveFile failed status = %d, code = %d\n", status, error);
        return -(int)error;
    } else {
        return 0;
    }
}


static int WINAPI
KfsLockFile(
    LPCWSTR             FileName,
    LONGLONG            ByteOffset,
    LONGLONG            Length,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsLockFile");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle;
    LARGE_INTEGER offset;
    LARGE_INTEGER length;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"LockFile %s\n", filePath);

    handle = (HANDLE)DokanFileInfo->Context;
    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");
        return -1;
    }

    length.QuadPart = Length;
    offset.QuadPart = ByteOffset;

    if (LockFile(handle, offset.HighPart, offset.LowPart, length.HighPart, length.LowPart)) {
        LOG(L"\tsuccess\n\n");
        return 0;
    } else {
        LOG(L"\tfail\n\n");
        return -1;
    }
}


static int WINAPI
KfsSetEndOfFile(
    LPCWSTR             FileName,
    LONGLONG            ByteOffset,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsSetEndOfFile");
    WCHAR           filePath[MAX_PATH];
    HANDLE          handle;
    LARGE_INTEGER   offset;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"SetEndOfFile %s, %I64d\n", filePath, ByteOffset);

    handle = (HANDLE)DokanFileInfo->Context;
    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");
        return -1;
    }

    offset.QuadPart = ByteOffset;
    if (!SetFilePointerEx(handle, offset, NULL, FILE_BEGIN)) {
        LOG(L"\tSetFilePointer error: %d, offset = %I64d\n\n",
                GetLastError(), ByteOffset);
        return GetLastError() * -1;
    }

    if (!SetEndOfFile(handle)) {
        DWORD error = GetLastError();
        LOG(L"\terror code = %d\n\n", error);
        return error * -1;
    }

    return 0;
}


static int WINAPI
KfsSetAllocationSize(
    LPCWSTR             FileName,
    LONGLONG            AllocSize,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsSetAllocationSize");
    WCHAR           filePath[MAX_PATH];
    HANDLE          handle;
    LARGE_INTEGER   fileSize;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"SetAllocationSize %s, %I64d\n", filePath, AllocSize);

    handle = (HANDLE)DokanFileInfo->Context;
    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");
        return -1;
    }

    if (GetFileSizeEx(handle, &fileSize)) {
        if (AllocSize < fileSize.QuadPart) {
            fileSize.QuadPart = AllocSize;
            if (!SetFilePointerEx(handle, fileSize, NULL, FILE_BEGIN)) {
                LOG(L"\tSetAllocationSize: SetFilePointer eror: %d, "
                    L"offset = %I64d\n\n", GetLastError(), AllocSize);
                return GetLastError() * -1;
            }
            if (!SetEndOfFile(handle)) {
                DWORD error = GetLastError();
                LOG(L"\terror code = %d\n\n", error);
                return error * -1;
            }
        }
    } else {
        DWORD error = GetLastError();
        LOG(L"\terror code = %d\n\n", error);
        return error * -1;
    }
    return 0;
}


static int WINAPI
KfsSetFileAttributes(
    LPCWSTR             FileName,
    DWORD               FileAttributes,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsSetFileAttributes");
    WCHAR   filePath[MAX_PATH];
    
    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"SetFileAttributes %s\n", filePath);

    if (!SetFileAttributes(filePath, FileAttributes)) {
        DWORD error = GetLastError();
        LOG(L"\terror code = %d\n\n", error);
        return error * -1;
    }

    LOG(L"\n");
    return 0;
}


static int WINAPI
KfsSetFileTime(
    LPCWSTR             FileName,
    CONST FILETIME*     CreationTime,
    CONST FILETIME*     LastAccessTime,
    CONST FILETIME*     LastWriteTime,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsSetFileTime");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"SetFileTime %s\n", filePath);

    handle = (HANDLE)DokanFileInfo->Context;

    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");
        return -1;
    }

    if (!SetFileTime(handle, CreationTime, LastAccessTime, LastWriteTime)) {
        DWORD error = GetLastError();
        LOG(L"\terror code = %d\n\n", error);
        return error * -1;
    }

    LOG(L"\n");
    return 0;
}


static int WINAPI
KfsUnlockFile(
    LPCWSTR             FileName,
    LONGLONG            ByteOffset,
    LONGLONG            Length,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsUnlockFile");
    WCHAR   filePath[MAX_PATH];
    HANDLE  handle;
    LARGE_INTEGER   length;
    LARGE_INTEGER   offset;

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"UnlockFile %s\n", filePath);

    handle = (HANDLE)DokanFileInfo->Context;
    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");
        return -1;
    }

    length.QuadPart = Length;
    offset.QuadPart = ByteOffset;

    if (UnlockFile(handle, offset.HighPart, offset.LowPart, length.HighPart, length.LowPart)) {
        LOG(L"\tsuccess\n\n");
        return 0;
    } else {
        LOG(L"\tfail\n\n");
        return -1;
    }
}


static int WINAPI
KfsGetFileSecurity(
    LPCWSTR                 FileName,
    PSECURITY_INFORMATION   SecurityInformation,
    PSECURITY_DESCRIPTOR    SecurityDescriptor,
    ULONG               BufferLength,
    PULONG              LengthNeeded,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsGetFileSecurity");
    HANDLE  handle;
    WCHAR   filePath[MAX_PATH];

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"GetFileSecurity %s\n", filePath);

    handle = (HANDLE)DokanFileInfo->Context;
    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");
        return -1;
    }

    if (!GetUserObjectSecurity(handle, SecurityInformation, SecurityDescriptor,
            BufferLength, LengthNeeded)) {
        int error = GetLastError();
        if (error == ERROR_INSUFFICIENT_BUFFER) {
            LOG(L"  GetUserObjectSecurity failed: ERROR_INSUFFICIENT_BUFFER\n");
            return error * -1;
        } else {
            LOG(L"  GetUserObjectSecurity failed: %d\n", error);
            return -1;
        }
    }
    return 0;
}


static int WINAPI
KfsSetFileSecurity(
    LPCWSTR                 FileName,
    PSECURITY_INFORMATION   SecurityInformation,
    PSECURITY_DESCRIPTOR    SecurityDescriptor,
    ULONG               SecurityDescriptorLength,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsSetFileSecurity");
    HANDLE  handle;
    WCHAR   filePath[MAX_PATH];

    kfs_t* kfs = (kfs_t*)DokanFileInfo->DokanOptions->GlobalContext;
    GetFilePath(kfs->_config, filePath, MAX_PATH, FileName);

    LOG(L"SetFileSecurity %s\n", filePath);

    handle = (HANDLE)DokanFileInfo->Context;
    if (!handle || handle == INVALID_HANDLE_VALUE) {
        LOG(L"\tinvalid handle\n\n");
        return -1;
    }

    if (!SetUserObjectSecurity(handle, SecurityInformation, SecurityDescriptor)) {
        int error = GetLastError();
        LOG(L"  SetUserObjectSecurity failed: %d\n", error);
        return -1;
    }
    return 0;
}

static int WINAPI
KfsGetVolumeInformation(
    LPWSTR      VolumeNameBuffer,
    DWORD       VolumeNameSize,
    LPDWORD     VolumeSerialNumber,
    LPDWORD     MaximumComponentLength,
    LPDWORD     FileSystemFlags,
    LPWSTR      FileSystemNameBuffer,
    DWORD       FileSystemNameSize,
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    TRACER(L"KfsGetVolumeInformation");
    wcscpy_s(VolumeNameBuffer, VolumeNameSize / sizeof(WCHAR), L"DOKAN");
    *VolumeSerialNumber = 0x19831116;
    *MaximumComponentLength = 256;
    *FileSystemFlags = FILE_CASE_SENSITIVE_SEARCH | 
                        FILE_CASE_PRESERVED_NAMES | 
                        FILE_SUPPORTS_REMOTE_STORAGE |
                        FILE_UNICODE_ON_DISK |
                        FILE_PERSISTENT_ACLS;

    wcscpy_s(FileSystemNameBuffer, FileSystemNameSize / sizeof(WCHAR), L"Dokan");

    return 0;
}


static int WINAPI
KfsUnmount(
    PDOKAN_FILE_INFO    DokanFileInfo)
{
    LOG(L"Unmount\n");
    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        case WM_DESTROY:
            // Exit the application when the window closes
            //TerminateThread(_kfs_thread, 0);
            _thread_count--;
            if (_thread_count <= 0 || (int)lParam!=66) {
                PostQuitMessage(1);
            }
            return true;
    }
    return DefWindowProc(hwnd,uMsg,wParam,lParam);
}

bool InitApp(HINSTANCE hInstance, LPSTR lpCmdLine)
{
    WNDCLASSEX wcx;

    // cbSize - the size of the structure.
    wcx.cbSize = sizeof(WNDCLASSEX);
    wcx.style = CS_HREDRAW | CS_VREDRAW;
    wcx.lpfnWndProc = (WNDPROC)WindowProc;
    wcx.cbClsExtra = 0;
    wcx.cbWndExtra = 0;
    wcx.hInstance = hInstance;
    wcx.hIcon = LoadIcon(hInstance, L"ki");
    wcx.hCursor = LoadCursor(NULL,IDC_ARROW);
    wcx.hbrBackground = (HBRUSH)(COLOR_BTNFACE + 1);
    wcx.lpszMenuName = NULL;
    wcx.lpszClassName = L"KFSCLS";
    wcx.hIconSm = LoadIcon(hInstance, L"ki");

    // Register the class with Windows
    if(!RegisterClassEx(&wcx))
        return false;

    return true;
}

HWND BuildWindow(int nCmdShow)
{
    DWORD style, xstyle;
    HWND retval;

    style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
    xstyle = WS_EX_LEFT;

    retval = CreateWindowEx(xstyle,
        L"KFSCLS",      // class name
        L"kfs", // title for our window (appears in the titlebar)
        style,
        CW_USEDEFAULT,  // initial x coordinate
        CW_USEDEFAULT,  // initial y coordinate
        200, 66,   // width and height of the window
        NULL,           // no parent window.
        NULL,           // no menu
        NULL,           // no creator
        NULL);          // no extra data

    if (retval == NULL) return NULL;  // BAD.

    xstyle = WS_EX_LEFT;
    style = WS_CHILD | WS_VISIBLE;
    HWND heightLabel = CreateWindowEx(
            xstyle, L"Static", 
            TITLE_STR, style,
            10, 10, 200, 50, 
            retval, NULL, NULL, NULL);

    HGDIOBJ hObj = GetStockObject(DEFAULT_GUI_FONT);
    SendMessage(heightLabel, WM_SETFONT, (WPARAM)hObj, true);

    //ShowWindow(retval,nCmdShow);  // Show the window
    ShowWindow(retval,
        SW_SHOWMINIMIZED|SW_SHOWMINNOACTIVE);  // Show the window
    return retval; // return its handle for future use.
}

void ReadConfiguration(list<config_t*>& configs)
{
    wchar_t config_ini[MAX_PATH];
    GetCurrentDirectory(MAX_PATH, config_ini);
    wcscat(config_ini, L"\\config.ini");

    wchar_t names[1024];
    size_t names_len = sizeof(names)/sizeof(wchar_t);
    GetPrivateProfileSectionNames(names, names_len, config_ini);

    wchar_t *p = names;
    while (p && *p) {
        wstring name(p);
        config_t* p_config = new config_t(name, config_ini);
        configs.push_back(p_config);

        p += wcslen(p) + 1;
    }
}

DWORD WINAPI dokan_main( kfs_t* kfs );

///*
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
    MSG msg; int retval;

    // init common controls
    //InitComCtls();

    if(InitApp(hInstance, lpCmdLine) == false)
        return 0;

    hWnd = BuildWindow(nCmdShow);
    if(hWnd == NULL)
        return 0;

    //SetPriorityClass(GetCurrentProcess(), IDLE_PRIORITY_CLASS);

    ReadConfiguration(_config_list);

    bool debug_processed(false);
    for (list<config_t*>::iterator it = _config_list.begin();
            it != _config_list.end();
            it++) {
        // Open Log file once
        if (!debug_processed) {
            g_DebugMode |= (*it)->_debug;
            g_UseStdErr = g_DebugMode;

            if (g_UseStdErr && !(*it)->_log_file.empty()) {
                _wfreopen((*it)->_log_file.c_str(), L"wt", stderr);
            }
            debug_processed = true;
            LOG(L"Log started\n");
        }

        // check for sections that do not have enough info
        if ((*it)->_afs_source.empty()) {
            continue;
        }
        if ((*it)->_kfs_mount.empty()) {
            continue;
        }

        // init a context
        kfs_t* kfs_context = new kfs_t(*it);

        HANDLE kfs_thread;
        DWORD kfs_thread_id;

        kfs_thread = CreateThread( 
                NULL,                   // default security attributes
                0,                      // use default stack size  
                (LPTHREAD_START_ROUTINE)dokan_main,  // thread function name
                kfs_context,            // argument to thread function 
                0,                      // use default creation flags 
                &kfs_thread_id);        // returns the thread identifier 

        _thread_count++;
    }

    while((retval = GetMessage(&msg,NULL,0,0)) != 0)
    {
        if(retval == -1)
            return 0;   // an error occured while getting a message

        // need to call this to make WS_TABSTOP work
        if (!IsDialogMessage(hWnd, &msg)) 
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return 0;
}
//*/

//int __cdecl
//wmain(ULONG argc, PWCHAR argv[])
DWORD WINAPI dokan_main( kfs_t* kfs )
{
    int status;
    ULONG command;
    PDOKAN_OPERATIONS dokanOperations =
            (PDOKAN_OPERATIONS)malloc(sizeof(DOKAN_OPERATIONS));
    PDOKAN_OPTIONS dokanOptions =
            (PDOKAN_OPTIONS)malloc(sizeof(DOKAN_OPTIONS));

    InitializeCriticalSection(&cs);
    InitializeCriticalSection(&logging_cs);

    RtlZeroMemory(dokanOptions, sizeof(DOKAN_OPTIONS));
    dokanOptions->Version = DOKAN_VERSION;
    dokanOptions->ThreadCount = 0; // use default
    dokanOptions->GlobalContext = (ULONG64)kfs;

    WCHAR RootDirectory[MAX_PATH] = L"C:";
    WCHAR MountPoint[MAX_PATH] = L"M:";

    // ThreadCount
    if (kfs->_config->_dokan_thread_count) {
        dokanOptions->ThreadCount = kfs->_config->_dokan_thread_count;
    }

    // RootDirectory
    if (!kfs->_config->_afs_source.empty()) {
        wcscpy_s(RootDirectory, sizeof(RootDirectory)/sizeof(WCHAR), 
            kfs->_config->_afs_source.c_str());
        LOG(L"RootDirectory: %ls\n", RootDirectory);
    }

    // MountPoint
    if (!kfs->_config->_kfs_mount.empty()) {
        wcscpy_s(MountPoint, sizeof(MountPoint)/sizeof(WCHAR), 
            kfs->_config->_kfs_mount.c_str());
        dokanOptions->MountPoint = MountPoint;
        LOG(L"MountPoint: %ls\n", MountPoint);
    }

    // Other options
    if (kfs->_config->_dokan_removable_drive) {
        dokanOptions->Options |= DOKAN_OPTION_REMOVABLE;
    }

    if (g_DebugMode) {
        dokanOptions->Options |= DOKAN_OPTION_DEBUG;
    }
    if (g_UseStdErr) {
        dokanOptions->Options |= DOKAN_OPTION_STDERR;
    }

    dokanOptions->Options |= DOKAN_OPTION_KEEP_ALIVE;

    RtlZeroMemory(dokanOperations, sizeof(DOKAN_OPERATIONS));
    dokanOperations->CreateFile = KfsCreateFile;
    dokanOperations->OpenDirectory = KfsOpenDirectory;
    dokanOperations->CreateDirectory = NULL; //KfsCreateDirectory;
    dokanOperations->Cleanup = KfsCleanup;
    dokanOperations->CloseFile = KfsCloseFile;
    dokanOperations->ReadFile = KfsReadFile;
    dokanOperations->WriteFile = KfsWriteFile; //NULL;
    dokanOperations->FlushFileBuffers = KfsFlushFileBuffers;
    dokanOperations->GetFileInformation = KfsGetFileInformation;
    dokanOperations->FindFiles = KfsFindFiles;
    dokanOperations->FindFilesWithPattern = NULL;
    dokanOperations->SetFileAttributes = KfsSetFileAttributes;
    dokanOperations->SetFileTime = KfsSetFileTime;
    dokanOperations->DeleteFile = KfsDeleteFile;
    dokanOperations->DeleteDirectory = KfsDeleteDirectory;
    dokanOperations->MoveFile = KfsMoveFile;
    dokanOperations->SetEndOfFile = KfsSetEndOfFile;
    dokanOperations->SetAllocationSize = KfsSetAllocationSize;  
    dokanOperations->LockFile = KfsLockFile;
    dokanOperations->UnlockFile = KfsUnlockFile;
    dokanOperations->GetFileSecurity = KfsGetFileSecurity;
    dokanOperations->SetFileSecurity = KfsSetFileSecurity;
    dokanOperations->GetDiskFreeSpace = NULL;
    dokanOperations->GetVolumeInformation = KfsGetVolumeInformation;
    dokanOperations->Unmount = KfsUnmount;

    status = DokanMain(dokanOptions, dokanOperations);

    switch (status) {
    case DOKAN_SUCCESS:
        LOG(L"Success\n");
        break;
    case DOKAN_ERROR:
        LOG(L"Error\n");
        break;
    case DOKAN_DRIVE_LETTER_ERROR:
        LOG(L"Bad Drive letter\n");
        break;
    case DOKAN_DRIVER_INSTALL_ERROR:
        LOG(L"Can't install driver\n");
        break;
    case DOKAN_START_ERROR:
        LOG(L"Driver something wrong\n");
        break;
    case DOKAN_MOUNT_ERROR:
        LOG(L"Can't assign a drive letter\n");
        break;
    case DOKAN_MOUNT_POINT_ERROR:
        LOG(L"Mount point error\n");
        break;
    default:
        LOG(L"Unknown error: %d\n", status);
        break;
    }

    //DeleteCriticalSection(&cs);
    //DeleteCriticalSection(&logging_cs);
    SendMessage(hWnd,WM_DESTROY,0,(LPARAM)66);

    free(dokanOptions);
    free(dokanOperations);

    LOG(L"FileSystem exiting.\n");
    fflush(stderr);
    return 0;
}

