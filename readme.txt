KFS: Kitserver File System
==========================
Version 0.6



Summary
=======
KFS is a user-mode file system application that provides a read-only "view"
of original AFS content, combined with files from "patch" directories 
(called "roots"). This allows for simple run-time patching of game content, 
while leaving the original AFS files unmodified.

AFS files are basically containers that encapsulate smaller content pieces 
(also called bins or files). These files typically have ".afs" or ".img" 
extension and can be found in many games developed by Konami, including
Winning Eleven and Pro Evolution Soccer. 



Prerequisites
=============
KFS requires Dokan Library 0.6.0 to be installed.
The installer is provided in the "dokan" folder - just run that and
follow the prompts. Should only take a few seconds.



Example usage
=============
We will use Pro Evolution Soccer 2013 DEMO as an example.
The included example configuration file (config.ini) assumes that the game 
is installed in C:\pes2013-demo. (If you have it in another location, you 
will need to modify config.ini accordingly). All steps need to be done
as Administrator.

1. rename the "C:\pes2013-demo\img" folder to "C:\pes2013-demo\img.real"
2. create a new empty folder called "C:\pes2013-demo\img"
3. start "kfs.exe"
4. start the game

The game should start normally, and you should see the new content while
playing: a Euro 2012 Adidas Tango ball used instead of Konami's "WE-PES 2013"
ball and also if you're playing on Medium or High settings, the blurring 
effect during the gameplay should no longer be there.



Multiple roots
==============
You can specify several "kfs.root" settings in your config.ini.
The example file refers to 2 roots: example-root1 and example-root2. The end
result is the combined content from all roots. If the same BIN exists in
multiple roots - as with the BIN #17 - then the root that is specified later
in the config.ini - wins. Try changing the order of "kfs.root" settings in 
the config.ini, and you will see that Speedcell ball will be used instead of 
Tango. 

Note that you need to restart kfs.exe anytime you make changes to 
config.ini. 

However, you do not need to restart kfs, if you just add/remove/modify files 
in existing roots. The changes will be automatically picked up, when the AFS
header section is read again. (PES only reads AFS header once, so you will 
need to restart the game to see the changes).


Documentation
=============
For more detailed documentation, please visit the web site:
https://sites.google.com/site/kitserverfs/



Credits:
========
Kitserver File System (KFS) 0.6
Copyright(c) 2012,2013 juce 

Dokan Library 0.6.0 
Copyright(c) Hiroki Asakawa http://dokan-dev.net/en

KFS Logo: by Ariel Santarelli

No-blur shaders (noblur_81.bin) by Tunizizou
Euro 2012 Adidas Tango ball (tango_ball_17.bin) by 1002MB
Adidas SpeedCell ball (speedcell_ball_17.bin) by BALKAN PES BOX team

